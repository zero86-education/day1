import React, {useState} from 'react';

export default function EventEx() {

    // state 정의
    const [num, setNum] = useState(0);

    // event 핸들러 함수 정의
    const handleIncrement = () => {
        // 상태 변경 함수 호출
        setNum(num + 1);
    };

    const handleDecrement = () => {
        // 상태 변경 함수 호출
        setNum(num - 1);
    };

    return (
        <div>
            <p>
                {num}
            </p>
            <button onClick={handleIncrement}>increment</button>
            <button onClick={handleDecrement}>decrement</button>
        </div>
    );
}