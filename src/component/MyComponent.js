import React from 'react';

// MyComponent
function MyComponent(props) {
    return <div>Hello, {props.name}</div>;
}

export default MyComponent; //다른 JS파일에서 불러올 수 있도록 내보내주기