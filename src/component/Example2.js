// react 패키지를 불러옵니다.
import React from 'react';


// 일반적인 함수 구문만으로 정의
function Example2() {
    return (
        <div>
            Example2 Function Component
        </div>
    );
}

// or
// const Example2 = () => <div>Example2 Function Component</div>

export default Example2