// react 패키지를 불러옵니다.
import React from 'react';


// Component 를 상속받는 Example 클래스를 작성합니다.
class Example extends React.Component {

    // render() 를 정의합니다.
    // 이는 정의한 컴포넌트를 렌더링하기 위한 메서드 입니다.
    // render() 는 JSX 또는 React 요소를 반환하기만 하는 처리를 담당 합니다.
    render() {
        return (
            <div>
                Example Class Component
            </div>
        );
    }
}

export default Example