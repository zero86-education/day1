import React from 'react';

function Greeting(props) {
    const {name} = props;
    return (
        <p>
            Hello, my name is {name}
        </p>
    );
}

export default function Hello() {
    return (
        <div>
            <Greeting name="홍길동"/>
        </div>
    );
}
