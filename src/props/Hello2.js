import React from 'react';

function Greeting(props) {
    // 구조분해 할당
    const {name, age, color} = props;
    return (
        <p style={{color}}>
            Hello, my name is {name} <br/>
            my age is {age}
        </p>
    );
}

export default function Hello2() {
    return (
        <div>
            <Greeting name="홍길동" age={20} color='blue'/>
        </div>
    );
}
