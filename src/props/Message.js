import React from 'react';

function Wrapper({children}) {
    const style = {
        border: '2px solid black',
        padding: '16px',
    };
    return (
        <div style={style}>
            {children}
        </div>
    );
}

function Message() {
    // return (
    //     <Wrapper>
    //         <p>
    //             Message Component
    //         </p>
    //     </Wrapper>
    // );

    // 명시적으로 전달
    return(
        <Wrapper children={<p>Message Component</p>}/>
    )
}
export default Message;