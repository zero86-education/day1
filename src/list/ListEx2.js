import React, {useState} from 'react';

export default function ListEx2() {
    const [arr, setArr] = useState([]);

    const onAddStart = () =>
        setArr(prev => [{id: prev.length + 1, createdAt: new Date()}, ...prev]);

    const onAddEnd = () =>
        setArr(prev => [...prev, {id: prev.length + 1, createdAt: new Date()}]);

    const onSortEarliest = () => {
        const sortedArray = arr.sort((a, b) => a.createdAt.getTime() - b.createdAt.getTime());
        setArr([...sortedArray]);
    };

    const onSortByLatest = () => {
        const sortedArray = arr.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
        setArr([...sortedArray]);
    };

    const onClear = () => setArr([]);

    return (
        <div>
            <div>
                <button onClick={onAddStart}>앞에 추가</button>
                <button onClick={onAddEnd}>뒤에 추가</button>
                <button onClick={onSortEarliest}>시간 정렬 (내림)</button>
                <button onClick={onSortByLatest}>시간 정렬 (오름)</button>
                <button onClick={onClear}>초기화</button>
            </div>
            {arr.map((item, index) => (
                <div key={index}>
                    <span>id : {item.id}</span>
                    <input/>
                </div>
            ))}
        </div>
    );
};