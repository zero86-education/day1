import React from 'react';
export default function ListEx() {
    const listItems = [1,2,3,4,5];

    return (
        <div>
            {listItems.map(item => <p>{item}</p>)}
        </div>
    )
}

// export default function ListEx() {
//     const listItems = [1,2,3,4,5];
//
//     return (
//         <div>
//             {listItems.map((item, index) => <p key={index}>{item}</p>)}
//         </div>
//     )
// }