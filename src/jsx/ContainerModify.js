import React from 'react';

export default function ContainerModify() {
    return (
        <div>
            <p style={{color: 'red'}}>
                안녕하세요?
            </p>
            <button onClick={() => alert('무얏호!')}>
                무얏호 버튼
            </button>
        </div>
    )
}