import React from 'react';

export default function Container() {
    return React.createElement('div',
        null,
        React.createElement(
            'p',
            {style: {color: 'red'}},
            '안녕하세요?'
        ),
        React.createElement('button',
            {
                onClick: () => alert('무얏호!')
            }, '무얏호 버튼',
        ),
    );
}